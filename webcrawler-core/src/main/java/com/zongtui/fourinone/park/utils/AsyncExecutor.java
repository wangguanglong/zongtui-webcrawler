package com.zongtui.fourinone.park.utils;

import com.zongtui.fourinone.utils.log.LogUtil;

public abstract class AsyncExecutor {
	public abstract void task();
	public void run(){
		try{
			new Thread(new Runnable(){
				public void run(){
					task();
				}
			}).start();
		}catch(Exception e){
			//e.printStackTrace();
			LogUtil.info("AsyncExecutor", "task", e);
		}
	}
}