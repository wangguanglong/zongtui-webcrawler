package com.zongtui.fourinone.worker;

import com.zongtui.fourinone.base.config.ConfigContext;
import com.zongtui.fourinone.file.WareHouse;
import com.zongtui.fourinone.contractor.ParallelService;

public abstract class WorkerParallel extends ParallelService
{
	protected int parallelPatternFlag = ConfigContext.getParallelPattern();
	
	//start workerservice or getlastest from park by keyid&workertype and invoke doTask get WareHouse to park and getlastest;
	public void waitWorking(String host, int port, String workerType)
	{
		if(parallelPatternFlag==1)
			waitWorkingByPark(workerType);
		else
			waitWorkingByService(host, port, workerType);
	}
	
	public void waitWorking(String workerType)
	{
		if(parallelPatternFlag==1)
			waitWorkingByPark(workerType);
		else
			waitWorkingByService(workerType);
	}
	
	protected void waitWorking(String parkhost, int parkport, String host, int port, String workerType)
	{
		waitWorkingByService(parkhost, parkport, host, port, workerType);
	}	
	
	public abstract void waitWorkingByService(String workerType);
	
	public abstract void waitWorkingByService(String host, int port, String workerType);
	
	public abstract void waitWorkingByService(String parkhost, int parkport, String host, int port, String workerType);
	
	public abstract void waitWorkingByPark(String workerType);
	
	/**
	 * getWorkerElse:获取除该工人外其它所有相同类型的工人. <br/>
	 *
	 * @author feng
	 * @return
	 * @since JDK 1.7
	 */
	protected abstract Workman[] getWorkerElse();
	
	/**
	 * getWorkerIndex:获取其它某个相同类型的工人，index为在集群中序号. <br/>
	 *
	 * @author feng
	 * @param index
	 * @return
	 * @since JDK 1.7
	 */
	protected abstract Workman getWorkerIndex(int index);
	
	/**
	 * getWorkerElse:获取除该工人外其它所有工人，workerType为工人类型. <br/>
	 *
	 * @author feng
	 * @param workerType
	 * @return
	 * @since JDK 1.7
	 */
	protected abstract Workman[] getWorkerElse(String workerType);
	
	/**
	 * getWorkerIndex:获取其它某个工人，workerType为工人类型，index为在集群中序号. <br/>
	 *
	 * @author feng
	 * @param workerType
	 * @param index
	 * @return
	 * @since JDK 1.7
	 */
	protected abstract Workman getWorkerIndex(String workerType, int index);
	
	/**
	 * getWorkerElse:获取某台机器上的工人. <br/>
	 *
	 * @author feng
	 * @param workerType 工人类型
	 * @param host ip
	 * @param port 端口
	 * @return
	 * @since JDK 1.7
	 */
	protected abstract Workman getWorkerElse(String workerType, String host, int port);
	
	/**
	 * getWorkerAll:获取集群中所有工人，包括该工人自己. <br/>
	 *
	 * @author feng
	 * @return
	 * @since JDK 1.7
	 */
	protected abstract Workman[] getWorkerAll();
	
	/**
	 * getWorkerAll:获取集群中所有类型为workerType的工人. <br/>
	 *
	 * @author feng
	 * @param workerType 类型
	 * @return
	 * @since JDK 1.7
	 */
	protected abstract Workman[] getWorkerAll(String workerType);
	
	/**
	 * getSelfIndex:获取自己在集群中的位置序号. <br/>
	 *
	 * @author feng
	 * @return
	 * @since JDK 1.7
	 */
	protected abstract int getSelfIndex();
	
	/**
	 * receive:接收来自其它工人的发送内容. <br/>
	 *
	 * @author feng
	 * @param inhouse
	 * @return
	 * @since JDK 1.7
	 */
	protected abstract boolean receive(WareHouse inhouse);

}